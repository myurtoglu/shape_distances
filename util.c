#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

void my_error(char* msg)
{
   fprintf(stderr, "%s: %s\n", msg, strerror(errno));
   exit(1);
}

// python style modulus
double mod2(double x, double y)
{
   return x > 0 ? fmod(x, y) : y + fmod(x, y);
}
