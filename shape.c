#include <math.h>
#include <stdlib.h>
#include "shape.h"
#include "util.h"

#define TOL 1e-2

Point* my_get_verts_of_shape(Shape *s)
{
    Point *pts = malloc(4 * sizeof(*pts));
    float th = s->heading;
    float sw = s->width;
    float sh = s->height;
    pts[0].x = s->sx - sw*cos(th)/2 - sh*sin(th)/2;
    pts[0].y = s->sy - sw*sin(th)/2 + sh*cos(th)/2;

    pts[1].x = s->sx - sw*cos(th)/2 + sh*sin(th)/2;
    pts[1].y = s->sy - sw*sin(th)/2 - sh*cos(th)/2;

    pts[2].x = s->sx + sw*cos(th)/2 + sh*sin(th)/2;
    pts[2].y = s->sy + sw*sin(th)/2 - sh*cos(th)/2;

    pts[3].x = s->sx + sw*cos(th)/2 - sh*sin(th)/2;
    pts[3].y = s->sy + sw*sin(th)/2 + sh*cos(th)/2;

    return pts;
}

float my_pt_dist(Point *p1, Point *p2)
{
    return sqrt(pow(p1->x - p2->x, 2) + pow(p1->y - p2->y, 2));
}

Point* my_get_closest_verts(Shape *s1, Shape *s2)
{
    Point *res = malloc(2 * sizeof(*res));
    Point *s1vertices = my_get_verts_of_shape(s1);
    Point *s2vertices = my_get_verts_of_shape(s2);
    float min_dist = 1e100;
    float temp_dist = 0.0f;
    int i = 0;
    for (i = 0; i < 4; ++i) {
        int j = 0;
        for (j = 0; j < 4; ++j) {
            temp_dist = my_pt_dist(&s1vertices[i], &s2vertices[j]);
            if (temp_dist < min_dist) {
                min_dist = temp_dist;
                res[0] = s1vertices[i];
                res[1] = s2vertices[j];
            }
        }
    }
    free(s1vertices);
    free(s2vertices);
    return res;
}

float my_dot_prod(Vector *v1, Vector *v2)
{
    return v1->x * v2->x + v1->y * v2->y;
}

float my_norm(Vector *v)
{
    return sqrt(my_dot_prod(v, v));
}

Vector* vec_from_pts(Point *p1, Point *p2)
{
    Vector *res = malloc(sizeof(*res));
    res->x = p2->x - p1->x;
    res->y = p2->y - p1->y;
    return res;
}

Vector* my_get_vec_diff(Vector *v1, Vector *v2)
{
    Vector *res = malloc(sizeof(*res));
    res->x = v2->x - v1->x;
    res->y = v2->y - v1->y;
    return res;
}

float my_vec_dist(Vector *v1, Vector *v2)
{
    Vector *diff_vec = my_get_vec_diff(v1, v2);
    float res =  my_norm(diff_vec);
    my_free_vec(&diff_vec);
    return res;
}

Point* my_get_closest_pt_to_seg(Point *p, Segment *s)
{
    Vector *v = vec_from_pts(&s->P0, &s->P1);
    Vector *w = vec_from_pts(&s->P0, p);
    Point *pret = malloc(sizeof(*pret));

    float c1 = my_dot_prod(w, v);
    if (c1 <= 0) {
        pret->x = s->P0.x;
        pret->y = s->P0.y;
        my_free_vec(&v);
        my_free_vec(&w);
        return pret;
    }

    float c2 = my_dot_prod(v, v);
    if (c2 <= c1) {
        pret->x = s->P1.x;
        pret->y = s->P1.y;
        my_free_vec(&v);
        my_free_vec(&w);
        return pret;
    }

    double b = c1 / c2;
    pret->x = s->P0.x + b * v->x;
    pret->y = s->P0.y + b * v->y;

    my_free_vec(&v);
    my_free_vec(&w);
    return pret;
}

float my_closest_pt_to_seg_dist(Point *p, Segment *s)
{
    Point *pb = my_get_closest_pt_to_seg(p, s);
    float ret = my_pt_dist(p, pb);
    my_free_pt(&pb);
    return ret;
}

Point* my_get_neigh_verts(Shape *s, Point *p)
{
    Point *res = malloc(2 * sizeof(*res));
    Point *candidates = my_get_verts_of_shape(s);
    Point *away_pt = malloc(sizeof(*away_pt));

    int i;
    float max_dist = 0.0f;
    float temp_dist = 0.0f;

    for (i = 0; i < 4; ++i) {
        temp_dist = my_pt_dist(&candidates[i], p);
        if (temp_dist > max_dist) {
                max_dist = temp_dist;
        }
    }

    int j = 0;
    for (i = 0; i < 4; ++i) {
        temp_dist = my_pt_dist(&candidates[i], p);
        if (temp_dist > TOL && (max_dist - temp_dist) > TOL) {
            res[j] = candidates[i];
            ++j;
        }
    }

    free(candidates);
    my_free_pt(&away_pt);
    return res;
}

Segment* my_get_closest_segs(Shape *s1, Shape *s2)
{
    Segment *res = malloc(4 * sizeof(*res));
    Point *closest_verts = my_get_closest_verts(s1, s2);
    Point *neigh_pts_1 = my_get_neigh_verts(s1, &closest_verts[0]);
    Point *neigh_pts_2 = my_get_neigh_verts(s2, &closest_verts[1]);

    res[0].P0 = closest_verts[0];
    res[0].P1 = neigh_pts_1[0];
    res[1].P0 = closest_verts[0];
    res[1].P1 = neigh_pts_1[1];
    res[2].P0 = closest_verts[1];
    res[2].P1 = neigh_pts_2[0];
    res[3].P0 = closest_verts[1];
    res[3].P1 = neigh_pts_2[1];

    free(closest_verts);
    free(neigh_pts_1);
    free(neigh_pts_2);
    return res;
}

Point* my_get_closest_pts(Shape *s1, Shape *s2)
{
    Point *res = malloc(2 * sizeof(*res));
    Point *close_verts = malloc(6 * sizeof(*close_verts));
    Point *closest_verts = my_get_closest_verts(s1, s2);
    Point *neigh_pts_1 = my_get_neigh_verts(s1, &closest_verts[0]);
    Point *neigh_pts_2 = my_get_neigh_verts(s2, &closest_verts[1]);
    close_verts[0] = closest_verts[0];
    close_verts[1] = neigh_pts_1[0];
    close_verts[2] = neigh_pts_1[1];
    close_verts[3] = closest_verts[1];
    close_verts[4] = neigh_pts_2[0];
    close_verts[5] = neigh_pts_2[1];

    free(neigh_pts_1);
    free(neigh_pts_2);
    free(closest_verts);

    Segment *closest_segs = my_get_closest_segs(s1, s2);

    float min_dist = 1e100;
    float temp_dist = 0.0f;
    int i = 0;
    int j = 0;
    int k = 0;
    for (i = 0; i < 6; ++i) {
        j = i <= 2 ? 2 : 0;
        k = j + 2;
        for (; j < k; ++j) {
            temp_dist = my_closest_pt_to_seg_dist(&close_verts[i],
                                               &closest_segs[j]);

            if (temp_dist < min_dist) {
                Point *temp = my_get_closest_pt_to_seg(&close_verts[i],
                                                    &closest_segs[j]);
                min_dist = temp_dist;
                res[0] = close_verts[i];
                res[1] = *temp;
                my_free_pt(&temp);
            }
        }
    }

    free(close_verts);
    free(closest_segs);
    return res;
}

void my_free_vec(Vector **ss)
{
    void *s;

    if (ss == NULL) {
        my_error("Pointer not found!");
    }
    else {
        s = *ss;
        free(s);
        *ss = NULL;
    }
}

void my_free_pt(Point **ss)
{
    void *s;

    if (ss == NULL) {
        my_error("Pointer not found!");
    }
    else {
        s = *ss;
        free(s);
        *ss = NULL;
    }
}
