#include <stdio.h>
#include <string.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_color.h>
#include <allegro5/allegro_font.h>
#include "graphics.h"
#include "shape.h"

void my_draw_shape(Shape *s);

void my_update_graphics(Objects *obj)
{
   ALLEGRO_TRANSFORM identity;
   al_identity_transform(&identity);
   al_use_transform(&identity);
   int i = 0;
   for (i = 0; i < 2; i++)
      my_draw_shape(&obj->shapes[i]);
}

void my_draw_shape(Shape *s)
{
   ALLEGRO_TRANSFORM transform;
   al_identity_transform(&transform);
   al_rotate_transform(&transform, s->heading);
   al_translate_transform(&transform, s->sx, s->sy);
   al_use_transform(&transform);
   al_draw_rectangle(-s->width / 2.0f, -s->height / 2.0f, s->width / 2.0f,
                     s->height / 2.0f, al_color_name(s->color), 3.0f);
}

void my_draw_line(Point *p1, Point *p2)
{
   ALLEGRO_TRANSFORM identity;
   al_identity_transform(&identity);
   al_use_transform(&identity);
   al_draw_line(p1->x, p1->y, p2->x, p2->y, al_map_rgb(0, 255, 0), 1.0f);
}

void my_write_dist(Point *p1, Point *p2, float x_pos, float y_pos,
                    ALLEGRO_FONT *font)
{
   float distance = my_pt_dist(p1, p2);
   char text_string[18] = "Distance: ";
   char dist_string[8];
   sprintf(dist_string, "%2.2f", distance);
   strcat(text_string, dist_string);
   al_draw_text(font, al_map_rgb(0,255,0), x_pos, y_pos ,
                ALLEGRO_ALIGN_CENTRE, text_string);
}

void my_draw_verts(Shape *s)
{
   ALLEGRO_TRANSFORM identity;
   al_identity_transform(&identity);
   al_use_transform(&identity);
   Point* pts = my_get_verts_of_shape(s);
   int i = 0;
   for (i = 0; i < 4; i++) {
      al_draw_filled_circle(pts[i].x, pts[i].y, 3.0f, al_map_rgb(150,150,255));
   }
   free(pts);
}

void my_print_vec(char* msg, Vector *v)
{
   printf("%s(%7.2f, %7.2f)\n", msg, v->x, v->y);
}

void my_print_pt(char* msg, Point *p)
{
   printf("%s(%7.2f, %7.2f)\n", msg, p->x, p->y);
}

void my_print_seg(char* msg, Segment *seg)
{
   printf("%s (%f, %f)----(%f, %f)\n", msg, seg->P0.x, seg->P0.y, seg->P1.x,
          seg->P1.y);
}
