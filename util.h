#ifndef UTIL_H
#define UTIL_H

// general exit with message function
void   my_error(char* msg);

// python style modulus
double mod2(double x, double y);

#endif
