uname_S := $(shell sh -c 'uname -s 2>/dev/null || echo not')
CC = cc
CPPFLAGS = -Wall -Wextra -Wfloat-equal -pedantic -Wno-unused-label \
-Wno-gnu-zero-variadic-macro-arguments
CFLAGS += `pkg-config --cflags allegro-5.0 \
allegro_acodec-5.0 allegro_audio-5.0 allegro_color-5.0 allegro_dialog-5.0\
allegro_font-5.0 allegro_image-5.0 allegro_main-5.0 allegro_memfile-5.0\
allegro_primitives-5.0 allegro_ttf-5.0`
LDFLAGS += `pkg-config --libs allegro-5.0 \
allegro_acodec-5.0 allegro_audio-5.0 allegro_color-5.0 allegro_dialog-5.0\
allegro_font-5.0 allegro_image-5.0 allegro_main-5.0 allegro_memfile-5.0\
allegro_primitives-5.0 allegro_ttf-5.0`

# Run with make debug=1 to compile with -g flag
debug = 0
ifeq ($(debug), 0)
	CPPFLAGS += -O3
else
	CPPFLAGS += -g
endif

ifeq ($(uname_S), Linux)
	LDFLAGS += -lm
	CC += -std=c99
endif

.PHONY: clean clobber all run

all: main.exe

run: main.exe
	./main.exe

main.exe: main.o shape.o graphics.o util.o
	$(CC) $^ -o $@ $(LDFLAGS)

main.o: main.c shape.h graphics.h util.h
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $<

shape.o: shape.c shape.h
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $<

graphics.o: graphics.c graphics.h
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $<

util.o: util.c util.h
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $<

tests: tests.exe
	./tests.exe

tests.exe: tests.o shape.o util.o graphics.o
	$(CC) $(CPPFLAGS) $^ -o $@ $(LDFLAGS)

tests.o: tests.c dbg.h
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $<

clean:
	rm -rf *.o

clobber: clean
	rm -rf *.exe
