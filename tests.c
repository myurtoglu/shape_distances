#include <stdio.h>
#include <stdlib.h>
#include <allegro5/allegro.h>
#include "shape.h"
#include "graphics.h"
#include "dbg.h"
#include "util.h"

void shape_tests()
{
   log_info("Checking the shape module");
   Vector v1 = {.x = 1.5, .y = 2.2};
   Vector v2 = {.x = 1.9, .y = 3.2};
   Point p0 = {.x = 0.3f, .y = 3.8f};
   Point p1 = {.x = 10.0f, .y = 7.2f};
   Point p2 = {.x = 3.0f, .y = 4.0f};
   Segment *s = malloc(sizeof(*s));
   s->P0 = p0;
   s->P1 = p1;
   my_print_pt("p0: ", &p0);
   my_print_pt("p1: ", &p1);
   my_print_pt("p2: ", &p2);
   my_print_vec("v1: ", &v1);
   my_print_vec("v2: ", &v2);
   my_print_seg("segment: ", s);

   check(my_dot_prod(&v1, &v2) >= 0, "my_dot_prod failed");
   log_info("the dot product of v1 and v2 is %f", my_dot_prod(&v1, &v2));
   check(my_norm(&v2) >= 0, "my_norm failed");
   log_info("the norm of vector v2 is %f", my_norm(&v2));
   check(my_vec_dist(&v1, &v2) >= 0, "my_vec_dist failed");
   log_info("the distance of vectors v1 and v2 is %f", my_vec_dist(&v1, &v2));

   Point *p3 = my_get_closest_pt_to_seg(&p2, s);
   check(p3, "my_get_closest_pt_to_seg failed");

   check(my_pt_dist(&p2, p3) >= 0, "my_pt_dist failed");
   log_info("the distance from the point to the segment is %f",
            my_pt_dist(&p2, p3));

   // my_free_vec(&v3);
   free(s);
   s = NULL;
   my_free_pt(&p3);

   log_info("shape module succeeded");
   return;

error:
   log_err("shape module tests failed");
   return;
}

void util_tests()
{
   log_info("Checking the util module");

   check(mod2(-1, 4) - 3 < 1e-3, "mod2 failed");
   check(mod2(5, 4) - 1 < 1e-3, "mod2 failed");

   log_info("util module tests succeeded");
   return;

error:
   log_err("util module tests failed");
   return;
}


int main()
{
   shape_tests();
   util_tests();
   return 0;
}
