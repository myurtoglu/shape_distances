#ifndef GRAPHICS_H
#define GRAPHICS_H

struct ALLEGRO_FONT;
struct Point;
struct Shape;
struct Segment;
struct Objects;
struct Vector;

void my_update_graphics(struct Objects *obj);
void my_draw_line(struct Point *p1, struct Point *p2);
void my_write_dist(struct Point *p1, struct Point *p2, float x_pos, float y_pos,
                struct ALLEGRO_FONT *font);
void my_draw_verts(struct Shape *s);
void my_print_pt(char* msg, struct Point *p);
void my_print_seg(char* msg, struct Segment *seg);
void my_print_vec(char* msg, struct Vector *v);

#endif // GRAPHICS_H
