#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include "shape.h"
#include "graphics.h"
#include "util.h"

#define H_RES      1200
#define V_RES      800
#define ROT        0.075
#define ACC        4
#define REC_WIDTH  60
#define REC_HEIGHT 40

ALLEGRO_EVENT_QUEUE *event_queue;
ALLEGRO_TIMER       *timer;
ALLEGRO_DISPLAY     *display;
ALLEGRO_FONT        *font;
bool                done;

void init(void)
{
   if (!al_init())
      my_error("Allegro failed to initalize");

   if (!al_install_keyboard())
      my_error("Failed to install keyboard");

   timer = al_create_timer(1.0 / 60.0);
   if (!timer)
      my_error("Failed to create timer");

   al_set_new_display_flags(ALLEGRO_WINDOWED);

   display = al_create_display(H_RES, V_RES);
   if (!display)
      my_error("Failed to create display");

   event_queue = al_create_event_queue();
   if (!event_queue)
      my_error("Failed to create event queue");

   al_init_font_addon();
   al_init_ttf_addon();

   char *font_file = "monaco.ttf";
   font = al_load_ttf_font(font_file, 20, 0);
   if (!font)
      my_error("Failed to load the font file");

   al_register_event_source(event_queue, al_get_keyboard_event_source());
   al_register_event_source(event_queue, al_get_timer_event_source(timer));
   al_register_event_source(event_queue, al_get_display_event_source(display));

   done = false;
}

void shutdown()
{
	if (timer)
		al_destroy_timer(timer);

   if (display)
   	al_destroy_display(display);

   if (event_queue)
   	al_destroy_event_queue(event_queue);

   if (font)
      al_destroy_font(font);
}

void game_loop(Objects *obj)
{
   bool redraw = true;
   al_start_timer(timer);
   short thrust[2] = {0, 0};

   while (!done) {
      ALLEGRO_EVENT event;
      ALLEGRO_KEYBOARD_STATE keys;
      al_wait_for_event(event_queue, &event);
      al_get_keyboard_state(&keys);
      if (event.type == ALLEGRO_EVENT_TIMER) {
         redraw = true;
         int i = 0;
         for (i = 0; i < 2; i++) {
            obj->shapes[i].speed = thrust[i] * ACC;
            obj->shapes[i].sy = mod2(obj->shapes[i].sy - (cos(obj->shapes[i].heading) * obj->shapes[i].speed),
                                     V_RES);
            obj->shapes[i].sx = mod2(obj->shapes[i].sx + (sin(obj->shapes[i].heading) * obj->shapes[i].speed),
                                     H_RES);
         }
      }
      if(al_key_down(&keys, ALLEGRO_KEY_ESCAPE)) {
         done = true;
      }
      if(al_key_down(&keys, ALLEGRO_KEY_W)) {
         thrust[0] = 1;
      }
      else {
         thrust[0] = 0;
      }
      if(al_key_down(&keys, ALLEGRO_KEY_UP)) {
         thrust[1] = 1;
      }
      else {
         thrust[1] = 0;
      }
      if(al_key_down(&keys, ALLEGRO_KEY_A)) {
         obj->shapes[0].heading -= ROT;
      }
      if(al_key_down(&keys, ALLEGRO_KEY_D)) {
         obj->shapes[0].heading += ROT;
      }
      if(al_key_down(&keys, ALLEGRO_KEY_LEFT)) {
         obj->shapes[1].heading -= ROT;
      }
      if(al_key_down(&keys, ALLEGRO_KEY_RIGHT)) {
         obj->shapes[1].heading += ROT;
      }
      if(al_key_down(&keys, ALLEGRO_KEY_R)) {
         obj->shapes[0].width += 2;
      }
      if(al_key_down(&keys, ALLEGRO_KEY_F)) {
         obj->shapes[0].width -= 2;
      }
      if(al_key_down(&keys, ALLEGRO_KEY_PAD_PLUS)) {
         obj->shapes[1].width += 2;
      }
      if(al_key_down(&keys, ALLEGRO_KEY_PAD_MINUS)) {
         obj->shapes[1].width -= 2;
      }

      Point *closest_pts = my_get_closest_pts(&obj->shapes[0], &obj->shapes[1]);

      if (redraw && al_is_event_queue_empty(event_queue)) {
         redraw = false;
         al_clear_to_color(al_map_rgb(0, 0, 0));
         my_write_dist(&closest_pts[0], &closest_pts[1], 100, 10, font);
         my_update_graphics(obj);
         my_draw_line(&closest_pts[0], &closest_pts[1]);
         al_flip_display();
      }
      free(closest_pts);
   }
}

int main()
{
   Shape s1 = {H_RES / 4.0f, V_RES / 4.0f, REC_WIDTH, REC_HEIGHT,
               0, 0, 0, "fuchsia"};
   Shape s2 = {3 * H_RES / 4, 3 * V_RES / 4, REC_WIDTH, REC_HEIGHT,
               0, 0, 0, "yellow"};

   Objects *obj = malloc(sizeof(*obj));
   obj->shapes[0] = s1;
   obj->shapes[1] = s2;

   init();
   game_loop(obj);
   free(obj);
   shutdown();

   return 0;
}
